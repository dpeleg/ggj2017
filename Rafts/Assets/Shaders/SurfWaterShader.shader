Shader "GGJ/SurfWaterShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Scale("Scale", Float) = 1
		_Radius("Rad", Float) = 1
		_RadiusWidth("Width", Float) = 1
		_Center("Center", Vector) = (0,0,0,0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 customValue;
		};

		float4 _Color;
		float _Scale;
		float _CentersX[50];
		float _CentersY[50];
		float _Radiuses[50];
		//float _RadiusesWidth[50];
		float _Radius;
		float _RadiusWidth;
		float _Center;

		float VertHeightAnim(float4 vertPos, float2 center, float radius, float radWid)
		{
			float radiusSq = (vertPos.x - center.x) * (vertPos.x - center.x) + (vertPos.y - center.y) * (vertPos.y - center.y);
			float circle = radius * radius;

			if (radiusSq > circle - radWid && radiusSq < circle + radWid)
			{
				vertPos.z += sin(radiusSq - circle) * _Scale;
				return vertPos.z;
			}
			return vertPos.z;
		}

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);

			for (int i = 0; i < 50; i++)
			{
				if (_Radiuses[i] > 0)
				{
					float2 center;
					center.x = _CentersX[i];
					center.y = _CentersY[i];
					float offset = VertHeightAnim(v.vertex, center, _Radiuses[i], _RadiusWidth);
					v.vertex.z += offset;
					o.customValue = offset;
				}
			}
			/*float offset = VertHeightAnim(v.vertex, _Center, _Radius, _RadiusWidth);
			v.vertex.z += offset;
			o.customValue = offset;*/
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Normal.z += IN.customValue;

			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
