﻿Shader "GGJ/Water"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_TempRadius("Radius", Float) = 1
		_RadiusWidth("Width", Float) = 1
		_Scale("Scale", Float) = 1
		_Amplitude("Amp", Float) = 1
		_Center("Center", Vector) = (0,0,0,0)
		_TempPos("Temp Position", Float) = 1
		_TempWidth("Temp Width", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float2 _Center;
			float _TempRadius;
			float _RadiusWidth;
			float _TempPos;
			float _TempWidth;

			float _Amplitude;
			float _Scale;
			float _CenterX;
			float _CenterY;
			float _CentersX[3];
			float _CentersY[3];
			float _Radiuses[3];
			float _RadiusesWidth[3];

			float4 VertHeightAnim(float4 vertPos, float2 center, float radius, float radWid)
			{
				float radiusSq = (vertPos.x - center.x) * (vertPos.x - center.x) + (vertPos.y - center.y) * (vertPos.y - center.y);
				float radiusSq2 = (vertPos.x * vertPos.x) + (vertPos.y * vertPos.y);
				float circle = radius * radius;// *_Scale;

				if (radiusSq > circle - radWid && radiusSq < circle + radWid)
				{
					//vertPos.z += sin(_Amplitude + radiusSq2 + (vertPos.x * center.x) + (vertPos.y * center.y)) * _Scale;//1 - (d / someNum);
					vertPos.z += sin(radiusSq - circle) * _Scale;//1 - (d / someNum);
					//vertPos.z += 1 - (d / someNum);
					return vertPos;
				}
				return vertPos;
			}

			float4 TempSinCheck(float4 vertPos, float center, float radWid)
			{
				float rd = 3.1416 / radWid ;
				if(vertPos.x > center - radWid && vertPos.x < center + radWid)
					vertPos.z = sin(vertPos.x - center);
				return vertPos;
			}

			v2f vert (appdata v)
			{
				v2f o;
				/*for (int i = 0; i < 3; i++)
				{
					//if (_Centers[i] != null && _Radiuses[i] != null && _Radiuses[i] > 0 && _RadiusesWidth[i] != null && _RadiusesWidth[i] > 0)
					if (_Radiuses[i] > 0 && _RadiusesWidth[i] > 0)
					{
						float2 center;
						center.x = _CentersX[i];
						center.y = _CentersY[i];
						v.vertex = VertHeightAnim(v.vertex, center, _Radiuses[i], _RadiusesWidth[i]);
					}
				}*/
				//float2 center;
				//center.x = _CenterX;
				//center.y = _CenterY;
				v.vertex = VertHeightAnim(v.vertex, _Center, _TempRadius, _RadiusWidth);
				//v.vertex = TempSinCheck(v.vertex, _TempPos, _TempWidth);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				return col;
			}
			ENDCG
		}
	}
}
