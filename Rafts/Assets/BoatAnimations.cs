﻿using UnityEngine;
using System.Collections;

public class BoatAnimations : MonoBehaviour {

    public float RockingForce;
    public bool isDead = false;
    Animator animator;

	// Use this for initialization
	void Start () {
        animator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        animator.SetFloat("RockingForce", RockingForce);
        if(!animator.GetBool("Dead"))
            animator.SetBool("Dead", isDead);
	}
}
