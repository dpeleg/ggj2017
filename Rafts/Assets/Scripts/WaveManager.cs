﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

/// <remarks>The test code only really works in a 2D Unity project.</remarks>
public struct Wave
{
    public float amplitude;

    public float elapsedTime { get { return parent.simulationTime - startTime; } }

    /// <remarks>Unity units/s</remarks>    
    public float speed;

    public float length;

    public WaveManager parent;

    public Vector2 position;

    public float startTime;    

    public Wave Multiplied(float coefficient) {
        // Plane assumed square        
        return new Wave() {
            amplitude = amplitude * coefficient,
            speed = speed * coefficient,
            length = length * coefficient,
            parent = parent,
            position = position * coefficient,
            startTime = startTime
        };
    }    

    public float ValueAt(Vector2 point)
    {
        return ValueAt((point - position).magnitude);
    }

    public float ValueAt(float distance)
    {
        if (elapsedTime < 0 || distance > elapsedTime * speed)
            // Wave doesn't yet exist, or hasn't yet reached the point
            return 0f;

        distance -= elapsedTime * speed; 

        return amplitude * Mathf.Sin(2 * Mathf.PI * (0.25f + (distance / length))); // the 0.25f is to ensure a peak at t_0 at the wave's origin (sin(pi / 2) = 1)
    }
}

public class WaveManager : MonoBehaviour
{
    const float HalfWorldSize = 40f; // TODO: get from water object's bounding box
    /// <remarks>Coordinates passed to the shader should fall in the range [-HalfShaderPlaneSize, HalfShaderPlaneSize]. You'd think it'd be 1f, but it's not.</remarks>
    const float HalfShaderPlaneSize = 20f;

    WaterTest debugWaterTest;
    public GameObject debugSpherePrefab;
    [HideInInspector]
    public List<GameObject> debugSpheres = new List<GameObject>();

    Wave defaultWave;

    public List<Wave> waves = new List<Wave>();    

    public float simulationTime;

    public void Start() {
        defaultWave.speed = 1.5f;
        defaultWave.amplitude = 1f;
        defaultWave.length = 3f;

        debugWaterTest = GameObject.FindObjectOfType<WaterTest>();
    }

    public Vector2 PlanePos(Vector3 worldPos)
    {
        return new Vector2(worldPos.x, worldPos.z);
    }

    public void AddWave(Vector3 worldPos, float simulationTime, float amplitude = 1f) {
        waves.Add(new Wave() {
            parent = this,
            speed = defaultWave.speed,
            length = defaultWave.length,
            position = PlanePos(worldPos),
            startTime = simulationTime,
            amplitude = amplitude
        });
    }

    public void Update()
    {
        simulationTime = Time.time;

        var mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        // Debug input handling
        if (Input.GetKeyDown(KeyCode.C))
            waves.Clear();

        // Visualization        
        foreach (var s in debugSpheres)
            Destroy(s);
        debugSpheres.Clear();

        /*foreach (var w in waves) {
            var s = Instantiate<GameObject>(debugSpherePrefab);
            s.transform.position = WorldPos(w.position);
            debugSpheres.Add(s);
        }*/

        var pertinentWaves = waves.Where(w => w.startTime <= simulationTime).Select(w => w.Multiplied(HalfShaderPlaneSize / HalfWorldSize)).ToArray(); // filter and resize       
        for (int i = 0; i < pertinentWaves.Count(); ++i)
            pertinentWaves[i].position /= 4.2f;
        debugWaterTest.xs = pertinentWaves.Select(w => w.position.x).ToArray();
        debugWaterTest.ys = pertinentWaves.Select(w => w.position.y).ToArray();
        debugWaterTest.rads = pertinentWaves.Select(w => w.speed * w.elapsedTime).ToArray();
        debugWaterTest.radsw = pertinentWaves.Select(w => w.length).ToArray();
        debugWaterTest.SetShader();
    }

    public float ValueAt(Vector2 point)
    {
        return waves.Sum(w => w.ValueAt(point));
    }

    public Vector3 WorldPos(Vector2 planePos)
    {   
        return new Vector3(planePos.x, 0f, planePos.y);                
    }
}
