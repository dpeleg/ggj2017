using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    public Transform PlayersListHolder;
    public GameObject PlayerListItemPrefab;
    public int PlayersLimit;
    public Player[] Players;

    private Stack<PlyaerItemList> _players = new Stack<PlyaerItemList>();

    public void AddPlayer()
    {
        if(_players.Count >= PlayersLimit) return;
        var item = Instantiate(PlayerListItemPrefab);
        item.transform.SetParent(PlayersListHolder);
        var component = item.GetComponent<PlyaerItemList>();
        component.Set(Players[_players.Count]);
        _players.Push(component);
    }

    public void RemovePlayer()
    {
        if(_players.Count == 0) return;
        var item = _players.Pop();
        Destroy(item.gameObject);
    }

    public void StartGame()
    {
        if (_players.Count < 2)
        {
            Debug.Log("Can't start game with less then 2 players");
            return;
        }
        var playersPlaying = FindObjectOfType<PlayerPlayingHolder>();
        var list = new List<Player>();
        var array = _players.ToArray().Reverse().ToArray();
        for (int i = 0; i < array.Length; i++)
        {
            list.Add(array[i].PlayerObject);
        }
        playersPlaying.PlayersPlaying = list.ToArray();
        SceneManager.LoadScene("Scene1");
    }
}
