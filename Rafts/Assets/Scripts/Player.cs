﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class Player : ScriptableObject
{
    public string PlayerId;
    public Color PlayerColor;
    public bool IsAlive;

    public override string ToString()
    {
        return string.Format("PlayerId: {0}, PlayerColor: {1}, IsAlive: {2}", PlayerId, PlayerColor, IsAlive);
    }
}
