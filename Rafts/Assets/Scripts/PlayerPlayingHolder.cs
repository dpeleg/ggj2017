using UnityEngine;
using System.Collections;

public class PlayerPlayingHolder : MonoBehaviour
{
    public Player[] PlayersPlaying;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
}
