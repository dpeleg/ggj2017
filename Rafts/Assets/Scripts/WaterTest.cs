using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTest : MonoBehaviour {

    public float[] xs;
    public float[] ys;
    public float[] rads;
    public float[] radsw;
    MaterialPropertyBlock matBlock;
    Renderer ren;

    void Start()
    {
        ren = GetComponent<Renderer>();
        matBlock = new MaterialPropertyBlock();
        SetDefaultFields();
    }

	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SetDefaultFields1();
        }
        if (Input.GetKeyDown(KeyCode.D)) {
            SetDefaultFields2();
        }
    }


    public void SetShader()
    {
        if (xs.Length == 0) 
            SetDefaultFields();
        
        matBlock.SetFloatArray("_CentersX", xs);
        matBlock.SetFloatArray("_CentersY", ys);
        matBlock.SetFloatArray("_Radiuses", rads);
        matBlock.SetFloatArray("_RadiusesWidth", radsw);
        ren.SetPropertyBlock(matBlock);
        
    }

    void SetDefaultFields() {
        xs = new float[50];
        ys = new float[50];
        rads = new float[50];
        radsw = new float[50];
    }

    void SetDefaultFields1() {
        xs[0] = 0f;
        ys[0] = 0f; 
        rads[0] = 2f;
        radsw[0] = 1.3f;
        SetShader();

    }

    void SetDefaultFields2() {
        xs[0] = 1f;
        ys[0] = 1f;
        rads[0] = 2f;
        radsw[0] = 1.3f;
        SetShader();
    }
}
