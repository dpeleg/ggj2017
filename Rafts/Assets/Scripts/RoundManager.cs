using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

public class RoundManager : MonoBehaviour
{
    public GameObject FPSController;
    public FirstPersonController FirstPersonController;
    public GameObject[] playerPrefabs;

    // the number in seconds a single turn takes
    public float TimePerTurn;
    public float TimeBetweenTurns;
    public Text CurrentPlayerLabel;
    public Text CurrentTimeLabel;


    // the list of all players in the game
    private Player[] _players;
    private CursorController cursorController;
    private Dictionary<Player, GameObject> playersDictionary;
    // a queue representing the turns
    private Queue<Player> _turns = new Queue<Player>();
    // should the turn timer run
    private bool _runTimer;
    public float _currentTime; // TEMP
    private Player _currentPlayer;
    //private int _turnsCount;
    //private int _currentPlayersCount;

	// Use this for initialization
	void Start()
	{
	    var playersPlaying = FindObjectOfType<PlayerPlayingHolder>();
	    _players = playersPlaying.PlayersPlaying;
        playersDictionary = new Dictionary<Player, GameObject>();
        cursorController = FPSController.GetComponentInChildren<CursorController>();
        // set up the inital turns
        //_currentPlayersCount = Players.Length;
        for (int i = 0; i < _players.Length; i++)
	    {
            playersDictionary.Add(_players[i], playerPrefabs[i]);
            _turns.Enqueue(_players[i]);
	    }
	    StartNextTurnFlow();

    }

    private void StartNextTurnFlow()
    {
        Debug.Log(_turns.Count);
        if (_turns.Count == 0)
        {
            CurrentTimeLabel.text = "Showing Results...";
            CurrentPlayerLabel.text = "";
            StartCoroutine(WaitForSimulation());
            //_turnsCount = 0;
            return;
        }
        StartCoroutine(WaitForPlayerReady());
    }

    private void PlayNextTurn()
    {
        _currentTime = 0;
        _currentPlayer = _turns.Dequeue();
        while (!_currentPlayer.IsAlive && _turns.Count > 0)
        {
            _currentPlayer = _turns.Dequeue();
            Debug.Log(_turns.Count);
        }

        cursorController.DeleteCursor();

        this.FPSController.transform.position = playersDictionary[_currentPlayer].transform.position;
        //this.FPSController.transform.forward = playersDictionary[_currentPlayer].transform.forward;
        this.FPSController.transform.rotation = playersDictionary[_currentPlayer].transform.rotation;
        this.FirstPersonController.enabled = true;
        Debug.Log(string.Format("removed [{0}] from queue", _currentPlayer));
        CurrentPlayerLabel.text = _currentPlayer.PlayerId;
        CurrentPlayerLabel.color = _currentPlayer.PlayerColor;
        //_turnsCount++;
        _runTimer = true;
    }

	// Update is called once per frame
	void Update()
	{

	    if (!_runTimer) return;
	    _currentTime += Time.deltaTime;
	    //TotalGameTime += Time.deltaTime;
        CurrentTimeLabel.text = (TimePerTurn - _currentTime).ToString("n1");
        if (_currentTime >= TimePerTurn)
	    {
            _runTimer = false;
            StartNextTurnFlow();
        }  
	}

    private IEnumerator WaitForSimulation()
    {
        yield return new WaitForSeconds(TimePerTurn);
        for (int i = 0; i < _players.Length; i++)
        {
            if (_players[i].IsAlive)
            {
                Debug.Log(string.Format("added [{0}] to queue", _players[i]));
                _turns.Enqueue(_players[i]);
            }
            else
            {
                var animation = playerPrefabs[i].GetComponent<BoatAnimations>();
                animation.isDead = true;
            }
        }
        if (_turns.Count == 1)
        {
            Debug.Log(string.Format("Winner is {0}", _turns.Peek()));
            yield break;
        }
        StartNextTurnFlow();
    }

    private IEnumerator WaitForPlayerReady()
    {
        this.FirstPersonController.enabled = false;

        CurrentTimeLabel.text = "Player get ready....";
        CurrentPlayerLabel.text = "";
        yield return new WaitForSeconds(TimeBetweenTurns);
        PlayNextTurn();
    }
}
