using UnityEngine;
using System.Collections;

public class CursorController : MonoBehaviour
{
    public GameObject cursorObject;
    public GameObject cursor;
    public GameObject emitterPrefab;
    [SerializeField]
    WaveManager waveManager;
    [SerializeField]
    RoundManager roundManager;


    public void DeleteCursor()
    {
        Destroy(cursor);
    }
	
	void Update ()
    {
        waveManager.simulationTime = roundManager._currentTime;

        if (Input.GetMouseButton(0))
        {
            //print("mouse down");
            Vector3 bombPosition; // = new Vector3();
            RaycastHit hit;
            Debug.DrawRay(transform.position, transform.forward, Color.red);

            if (Physics.Raycast(this.transform.position, transform.forward, out hit))
            {
                if (hit.transform.tag == "Water")
                {
                    //print("hit position: "+hit.point);
                    bombPosition = new Vector3(hit.point.x, hit.point.y + 0.05f, hit.point.z);                    
                    if (cursor != null)
                    {
                        //print("moving cursor.");
                        cursor.transform.position = bombPosition;
                    }
                    else
                    {
                        //print("creating cursor.");
                        cursor = Instantiate(cursorObject, bombPosition, cursorObject.transform.rotation) as GameObject;
                    }
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (cursor != null)
            {
                waveManager.AddWave(cursor.transform.position, waveManager.simulationTime);
                Instantiate(emitterPrefab, cursor.transform.position, Quaternion.identity);
            }
        }
	}
}
