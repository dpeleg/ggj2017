﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlyaerItemList : MonoBehaviour
{
    public Text PlayerId;
    public Image PlayerColor;
    public Player PlayerObject { get; private set; }

    public void Set(Player player)
    {
        PlayerObject = player;
        PlayerId.text = player.PlayerId;
        PlayerColor.color = player.PlayerColor;
    }
}
